<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Expense extends Model
{
    public function expensesReport(){
        return $this->belongsTo(ExpenseReport::class);
    }
}
